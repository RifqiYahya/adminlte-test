<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function index(){
        $questions = Pertanyaan::all();
        return view('pertanyaan.index', [
            'questions' => $questions
        ]);
    }

    public function show($id){
        $result = Pertanyaan::find($id);
        return view('pertanyaan.show', [
            'result' => $result
        ]);
    }

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){

        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        
        $query = new Pertanyaan();
        $query->judul = $request['judul'];
        $query->isi = $request['isi'];

        $query->save();
        
        return redirect('/pertanyaan');
    }

    public function edit($id){
        $target = Pertanyaan::find($id);
        return view('pertanyaan.edit',[
            'target' => $target
        ]);
    }

    public function update($id, Request $request){
        $request->validate([
            'judul'=>'required',
            'isi'=>'required'
        ]);

        $query = Pertanyaan::find($id);
        $query->judul = $request['judul'];
        $query->isi = $request['isi'];
        $query->update();
        
        $result = Pertanyaan::find($id);

        return redirect('/pertanyaan/'.$id);
    }

    public function destroy($id){
        $query = Pertanyaan::find($id);
        $query->delete();
        return redirect('/pertanyaan');
    }


}
