<div class="card">
  <a href="{{isset($link) ? $link : '#'}}" class='text-dark'>
    <div class="card-header" >
      <h3 class="card-title">{{ isset($title) ? $title : null }}</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button> --}}
      </div>
    </div>
  </a>
    <div class="card-body">
      {{ isset($body) ? $body : null}}
    </div>
    <!-- /.card-body -->
    <div class="card- footer">
      {{ isset($footer) ? $footer : null}}
    </div>
    <!-- /.card-footer-->
</div>