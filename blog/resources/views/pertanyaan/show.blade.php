@extends('layouts.master')
@section('header', $result->judul)
    
@section('content')
    <div class="card">  
      <div class="card-body h5">
        {{ $result->isi}}
      </div>
      <!-- /.card-body -->
      <div class="card- footer d-flex flex-row-reverse">
        <a href="/pertanyaan/{{$result->id}}/edit" id='edit'>
            <button type="submit" class="btn btn-primary mr-3 mb-3">Edit</button>
        </a>
        <form action="/pertanyaan/{{ $result->id }}" method='POST' id='delete-user'>
          @csrf
          @method('DELETE')
          <button id='delete' class="btn btn-primary mr-3 mb-3">Delete</button>
        </form>
      </div>
      <!-- /.card-footer-->
    </div>
@endsection

@push('scripts')
   <script>
       const button = document.getElementById('delete');
       button.addEventListener('click', function(e){
          e.preventDefault();
          let conf = confirm('Are you sure to delete this question?');
          if(conf){
            document.getElementById('delete-user').submit();
          }
       });
   </script>
@endpush