@extends('layouts.master')

@section('header', 'Create new question!')

@section('content')
    <div class="card">
      <div class="card-body">
        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="form-group">
                <label for="judul">Title</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Insert the Title Here">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Body</label>
                <textarea class="form-control" name="isi" id="isi" cols="30" rows="5" placeholder="Insert the Detail Here"></textarea>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-check" name= 'anCheckbox' >
                <input type="checkbox" id='anCheckbox' class="form-check-input" value='1' checked>
                <label for="anCheckbox" class="form-check-label">Send as Anonymous</label>
            </div>
            <div class="d-flex flex-row-reverse">
                <button type="submit" class="btn btn-primary">Post your question!</button>
            </div>
        </form>
      </div>
      <!-- /.card-body -->
      <div class="card- footer d-flex flex-row-reverse mr-3 mb-3">
        
      </div>
      <!-- /.card-footer-->
    </div>
@endsection