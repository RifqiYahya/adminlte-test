@extends('layouts.master')

@section('header', 'Question Page')

@section('content')
    <div class="d-flex flex-row-reverse mb-3">
        <a href="/pertanyaan/create">
            <button class="btn btn-primary">Ask new question</button>
        </a>
    </div>
    
    @forelse ($questions as $question)
        @component('layouts.partials.card')
            @slot('link', '/pertanyaan/'.$question->id)
            @slot('title', $question->judul)
            @slot('body', substr($question->isi, 0, 100).'...')
        @endcomponent
    @empty
        
    @endforelse
    
@endsection
