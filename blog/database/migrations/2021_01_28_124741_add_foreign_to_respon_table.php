<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToResponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respon_pertanyaaan', function (Blueprint $table) {
            $table->foreign('profil_id')->references('id')->on('profil');
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
        });

        Schema::table('respon_jawaban', function (Blueprint $table) {
            $table->foreign('profil_id')->references('id')->on('profil');
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respon_pertanyaaan', function (Blueprint $table) {
            $table->dropForeign(['profil_id']);
            $table->dropForeign(['pertanyaan_id']);
        });

        Schema::table('respon_jawaban', function (Blueprint $table) {
            $table->dropForeign(['profil_id']);
            $table->dropForeign(['jawaban_id']);
        });
    }
}
