<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->text('isi')->nullable($value=true)->change();
        });
        Schema::table('jawaban', function (Blueprint $table) {
            $table->text('isi')->nullable($value=true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->string('isi', 255)->change();
        });
        Schema::table('jawaban', function (Blueprint $table) {
            $table->string('isi', 255)->change();
        });
        
    }
}
